/**
 * @brief Test log.h, the log message interface
 *
 * @author: Bryant Hansen <sourcecode@bryanthansen.net>
 *
 * @license: LGPL
 */

// alternative to syslog - want to log to local files and not waste system resources
#include "log.h"

int main(int argc, char * argv[]) {

  /// @NOTE Currently has no effect; should allow to write to stdout, stderr, or
  ///       a file
  SET_LOG_FD(stderr)

  LOG_MSG("No Color\n");

  LOG_MSG("Test LOG_COLOR MACRO macros\n");
  LOG_COLOR(RED_TEXT, "RED_TEXT message %d\n", 0);
  LOG_COLOR(RED_TEXT, "RED_TEXT message (no args)\n");
  LOG_COLOR(CYAN_TEXT, "CYAN_TEXT message %d\n", 0);
  LOG_COLOR(CYAN_TEXT, "CYAN_TEXT message (no args)\n");
  LOG_COLOR(GREEN_TEXT, "GREEN_TEXT message %d\n", 0);
  LOG_COLOR(GREEN_TEXT, "GREEN_TEXT message (no args)\n");
  LOG_COLOR(YELLOW_TEXT, "YELLOW_TEXT message %d\n", 0);
  LOG_COLOR(YELLOW_TEXT, "YELLOW_TEXT message (no args)\n");

  fprintf(stderr, "\n");
  LOG_MSG("Test the explicit _MSG calls\n");
  EMERG_MSG("EMERG_MSG MACRO message %d\n", 0);
  EMERG_MSG("EMERG_MSG MACRO message (no args)\n");
  ALERT_MSG("ALERT_MSG MACRO message %d\n", 0);
  ALERT_MSG("ALERT_MSG MACRO message (no args)\n");
  CRIT_MSG("CRIT_MSG MACRO message %d\n", 0);
  CRIT_MSG("CRIT_MSG MACRO message (no args)\n");
  ERR_MSG("ERR_MSG MACRO message %d\n", 0);
  ERR_MSG("ERR_MSG MACRO message (no args)\n");

  fprintf(stderr, "\n");
  LOG_MSG("Loop through all LOG_LEVEL-as-argument calls\n");
  for (int level = 0;level < 10;level++) {
    LOG(level, "LOG(level == %d)\n", level);
    LOG(level, "LOG(level) macro -- (no args)\n");
  }
}
