# Bryant Hansen <sourcecode@bryanthansen.net>

.PHONY: testbin lint clean
# first target is default
testbin: test.c log.h
	gcc -I/usr/include -o $@ $<

test: testbin
	./$<

lint:
	cpplint test.c log.h

clean:
	rm -f testbin
