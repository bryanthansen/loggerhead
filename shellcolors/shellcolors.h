/**
 * log.h
 *
 * @author: Bryant Hansen <sourcecode@bryanthansen.net>
 *
 * @license: LGPL
 *
 * @brief test shell color logging from a C program
 */

#ifndef __LOG_H
#define __LOG_H

#include <stdio.h>

#define ESCAPE                  "\033"
#define COLOR_RESET             "\033[0m"
#define BOLD                    "\033[1m"
#define BLACK_TEXT              "\033[30;1m"
#define RED_TEXT                "\033[31;1m"
#define GREEN_TEXT              "\033[32;1m"
#define YELLOW_TEXT             "\033[33;1m"
#define BLUE_TEXT               "\033[34;1m"
#define MAGENTA_TEXT            "\033[35;1m"
#define CYAN_TEXT               "\033[36;1m"
#define WHITE_TEXT              "\033[37;1m"
#define RED_INVERTED_TEXT       "\033[41;1m"
#define RED_FLASHING_TEXT       "\033[5;31m"
#define RED_BG_FLASHING_TEXT    "\033[5;41m"

#define SHELL_COLOR_BLACK       "0"
#define SHELL_COLOR_RED         "1"
#define SHELL_COLOR_GREEN       "2"
#define SHELL_COLOR_YELLOW      "3"
#define SHELL_COLOR_BLUE        "4"
#define SHELL_COLOR_MAGENTA     "5"
#define SHELL_COLOR_CYAN        "6"
#define SHELL_COLOR_WHITE       "7"
#define SHELL_COLOR_BLACK8      "8"
#define SHELL_COLOR_BLACK9      "9"

#define MOD_NORMAL              0
#define MOD_BRIGHT              1
#define MOD_DARK                2
#define MOD_ITALIC              3
#define MOD_UNKNOWN_4           4
#define MOD_FLASHING            5
#define MOD_UNKNOWN_6           6
#define MOD_WHITE_BG            7
#define MOD_UNKNOWN_invisible_8 8
#define MOD_STRIKETHROUGH       9

#define LOG_COLOR(color, ...)              \
    fprintf(stderr, color) ;               \
    fprintf(stderr, __VA_ARGS__) ;         \
    fprintf(stderr, COLOR_RESET)

#endif
