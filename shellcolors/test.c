/**
 * @brief Test log.h, the log message interface
 *
 * @author: Bryant Hansen <sourcecode@bryanthansen.net>
 *
 * @license: LGPL
 */


/**
  \e[0;29m  \e[1;29m  \e[2;29m  \e[3;29m  \e[4;29m  \e[5;29m  \e[6;29m  \e[7;29m  \e[8;29m  \e[9;29m  
  \e[0;30m  \e[1;30m  \e[2;30m  \e[3;30m  \e[4;30m  \e[5;30m  \e[6;30m  \e[7;30m  \e[8;30m  \e[9;30m  
  \e[0;31m  \e[1;31m  \e[2;31m  \e[3;31m  \e[4;31m  \e[5;31m  \e[6;31m  \e[7;31m  \e[8;31m  \e[9;31m  
  \e[0;32m  \e[1;32m  \e[2;32m  \e[3;32m  \e[4;32m  \e[5;32m  \e[6;32m  \e[7;32m  \e[8;32m  \e[9;32m  
  \e[0;33m  \e[1;33m  \e[2;33m  \e[3;33m  \e[4;33m  \e[5;33m  \e[6;33m  \e[7;33m  \e[8;33m  \e[9;33m  
  \e[0;34m  \e[1;34m  \e[2;34m  \e[3;34m  \e[4;34m  \e[5;34m  \e[6;34m  \e[7;34m  \e[8;34m  \e[9;34m  
  \e[0;35m  \e[1;35m  \e[2;35m  \e[3;35m  \e[4;35m  \e[5;35m  \e[6;35m  \e[7;35m  \e[8;35m  \e[9;35m  
  \e[0;36m  \e[1;36m  \e[2;36m  \e[3;36m  \e[4;36m  \e[5;36m  \e[6;36m  \e[7;36m  \e[8;36m  \e[9;36m  
  \e[0;37m  \e[1;37m  \e[2;37m  \e[3;37m  \e[4;37m  \e[5;37m  \e[6;37m  \e[7;37m  \e[8;37m  \e[9;37m  
  \e[0;38m  \e[1;38m  \e[2;38m  \e[3;38m  \e[4;38m  \e[5;38m  \e[6;38m  \e[7;38m  \e[8;38m  \e[9;38m  
  \e[0;39m  \e[1;39m  \e[2;39m  \e[3;39m  \e[4;39m  \e[5;39m  \e[6;39m  \e[7;39m  \e[8;39m  \e[9;39m  
  \e[0;40m  \e[1;40m  \e[2;40m  \e[3;40m  \e[4;40m  \e[5;40m  \e[6;40m  \e[7;40m  \e[8;40m  \e[9;40m  
  \e[0;41m  \e[1;41m  \e[2;41m  \e[3;41m  \e[4;41m  \e[5;41m  \e[6;41m  \e[7;41m  \e[8;41m  \e[9;41m  
  \e[0;42m  \e[1;42m  \e[2;42m  \e[3;42m  \e[4;42m  \e[5;42m  \e[6;42m  \e[7;42m  \e[8;42m  \e[9;42m  
  \e[0;43m  \e[1;43m  \e[2;43m  \e[3;43m  \e[4;43m  \e[5;43m  \e[6;43m  \e[7;43m  \e[8;43m  \e[9;43m  
  \e[0;44m  \e[1;44m  \e[2;44m  \e[3;44m  \e[4;44m  \e[5;44m  \e[6;44m  \e[7;44m  \e[8;44m  \e[9;44m  
  \e[0;45m  \e[1;45m  \e[2;45m  \e[3;45m  \e[4;45m  \e[5;45m  \e[6;45m  \e[7;45m  \e[8;45m  \e[9;45m  
  \e[0;46m  \e[1;46m  \e[2;46m  \e[3;46m  \e[4;46m  \e[5;46m  \e[6;46m  \e[7;46m  \e[8;46m  \e[9;46m  
  \e[0;47m  \e[1;47m  \e[2;47m  \e[3;47m  \e[4;47m  \e[5;47m  \e[6;47m  \e[7;47m  \e[8;47m  \e[9;47m  
  \e[0;48m  \e[1;48m  \e[2;48m  \e[3;48m  \e[4;48m  \e[5;48m  \e[6;48m  \e[7;48m  \e[8;48m  \e[9;48m  
*/


// alternative to syslog - want to log to local files and not waste system resources
#include "shellcolors.h"

int main(int argc, char * argv[]) {
  char charspec[12];
  for (int bg = 3;bg <= 4;bg++) {
    for (int color = 0;color < 10;color++) {
      for (int mode = 0;mode < 10;mode++) {
        sprintf(charspec, "[%d;%d%dm", mode, bg, color);
        fprintf(stderr, "\033%s %s %s ", charspec, charspec, COLOR_RESET);
      }
      fprintf(stderr, "\n");
    }
  }
}
