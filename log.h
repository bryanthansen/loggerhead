/**
 * log.h
 *
 * @author: Bryant Hansen <sourcecode@bryanthansen.net>
 *
 * @license: LGPL
 *
 * @brief This is a general-purpose logging facility for programs written in
 * the C programming language on Linux systems.
 *
 * This has been written many times, but I could not find an off-the-shelf
 * version that has all of the features that I wanted.
 *
 * Feature List:
 *
 * - Supports multiple, standard log levels
 * - Color coded output for shell terminals which support color
 * - Automatically add timestamps to each message
 *
 * Shell Colors:
 * 
 * There are only 9 basic shell colors that the shell seems to support, at least
 * on the version I am using.
 * Some text attributes can also be enabled in various combinations:
 *   - some degree of control over the background color
 *   - bright and dim versions of foreground text colors
 *   - flashing text
 *   - underlines
 *   - strikethoughs
 *   - the bold property should exist, but I have not yet verified it on my
 *     current shell
 *
 */

#ifndef __LOG_H
#define __LOG_H

/**
 * LOG_EMERG	0	/* system is unusable *
 * LOG_ALERT	1	/* action must be taken immediately *
 * LOG_CRIT	2	/* critical conditions *
 * LOG_ERR		3	/* error conditions *
 * LOG_WARNING	4	/* warning conditions *
 * LOG_NOTICE	5	/* normal but significant condition *
 * LOG_INFO	6	/* informational *
 * LOG_DEBUG	7	/* debug-level messages *
 */
// #include <sys/syslog.h>

#include <stdio.h>
#include <time.h>

/**
 * @brief A copy of the LOG levels from Linux's sys/syslog.h
 *
 * @{
 */
#define	LOG_EMERG	0	   /* system is unusable */
#define	LOG_ALERT	1	   /* action must be taken immediately */
#define	LOG_CRIT	2	   /* critical conditions */
#define	LOG_ERR		3	   /* error conditions */
#define	LOG_WARNING	4	 /* warning conditions */
#define	LOG_NOTICE	5	 /* normal but significant condition */
#define	LOG_INFO	6	   /* informational */
#define	LOG_DEBUG	7	   /* debug-level messages */
 /** @{ */

#define DEFAULT_LOG_LEVEL LOG_WARN

#define COLOR_RESET              "\033[0m"
#define BOLD                     "\033[1m"
#define BLACK_TEXT               "\033[30;1m"
#define RED_TEXT                 "\033[31;1m"
#define GREEN_TEXT               "\033[32;1m"
#define YELLOW_TEXT              "\033[33;1m"
#define BLUE_TEXT                "\033[34;1m"
#define MAGENTA_TEXT             "\033[35;1m"
#define CYAN_TEXT                "\033[36;1m"
#define WHITE_TEXT               "\033[37;1m"
#define RED_INVERTED_TEXT        "\033[41;1m"
#define RED_FLASHING_TEXT        "\033[5;31m"
#define RED_BG_FLASHING_TEXT     "\033[5;41m"

/// @TODO implement this
#define SET_LOG_FD(x)

#define TIMESTAMP { \
    char buff[22]; \
    struct tm sTm; \
    time_t now = time (0); \
    gmtime_r (&now, &sTm); \
    strftime (buff, sizeof(buff), "[%Y-%m-%d_%H:%M:%S]:", &sTm); \
    fprintf(stderr, "%s: ", buff); \
}

#define LOG_MSG(...) TIMESTAMP fprintf(stderr, __VA_ARGS__) ;

#define LOG_COLOR(color, ...)              \
    fprintf(stderr, color) ;               \
    TIMESTAMP                              \
    fprintf(stderr, __VA_ARGS__) ;         \
    fprintf(stderr, COLOR_RESET)

#define LOG_COLOR_LEVEL(color, level, ...) \
    fprintf(stderr, color) ;               \
    TIMESTAMP                              \
    fprintf(stderr,  level ": " __VA_ARGS__) ; \
    fprintf(stderr, COLOR_RESET)

#if DEFAULT_LOG_LEVEL > LOG_EMERG
#define EMERG_MSG(...) while(0){}
#else
#define EMERG_MSG(...) LOG_COLOR_LEVEL(RED_BG_FLASHING_TEXT, \
                                       "EMERG", \
                                       __VA_ARGS__)
#endif

#if DEFAULT_LOG_LEVEL > LOG_ALERT
#define ALERT_MSG(...) while(0){}
#else
#define ALERT_MSG(...) LOG_COLOR_LEVEL(RED_INVERTED_TEXT, "ALERT", __VA_ARGS__)
#endif

#if DEFAULT_LOG_LEVEL > LOG_CRIT
#define CRIT_MSG(...) while(0){}
#else
#define CRIT_MSG(...) LOG_COLOR_LEVEL(RED_FLASHING_TEXT, "CRIT", __VA_ARGS__)
#endif

#if DEFAULT_LOG_LEVEL > LOG_ERR
#define ERR_MSG(...) while(0){}
#else
#define ERR_MSG(...) LOG_COLOR_LEVEL(RED_TEXT, "ERROR", __VA_ARGS__)
#endif

#if DEFAULT_LOG_LEVEL > LOG_WARNING
#define WARN_MSG(...) while(0){}
#else
#define WARN_MSG(...) LOG_COLOR_LEVEL(YELLOW_TEXT, "WARNING", __VA_ARGS__)
#endif

#if DEFAULT_LOG_LEVEL > LOG_NOTICE
#define NOTICE_MSG(...) while(0){}
#else
#define NOTICE_MSG(...) LOG_COLOR_LEVEL(MAGENTA_TEXT, "NOTICE", __VA_ARGS__)
#endif

#if DEFAULT_LOG_LEVEL > LOG_INFO
#define INFO_MSG(...) while(0){}
#else
#define INFO_MSG(...) LOG_COLOR_LEVEL(WHITE_TEXT, "INFO", __VA_ARGS__)
#endif

#if DEFAULT_LOG_LEVEL > LOG_DEBUG
#define DEBUG_MSG(...) while(0){}
#else
#define DEBUG_MSG(...) LOG_COLOR_LEVEL(GREEN_TEXT, "DEBUG", __VA_ARGS__)
#endif

#define INVALID_LEVEL_MSG(...) LOG_COLOR_LEVEL(WHITE_TEXT, \
                                               "INVALID_LOG_LEVEL", \
                                               __VA_ARGS__)

/// @NOTE: provide a transformation for providing the level as an argument
/// @TODO: move this from the C-code domain back into the preprocessor domain
#define LOG(LEVEL, ...) \
    if      (LEVEL == LOG_EMERG)   { EMERG_MSG(__VA_ARGS__); } \
    else if (LEVEL == LOG_ALERT)   { ALERT_MSG(__VA_ARGS__); } \
    else if (LEVEL == LOG_CRIT)    { CRIT_MSG(__VA_ARGS__); } \
    else if (LEVEL == LOG_ERR)     { ERR_MSG(__VA_ARGS__); } \
    else if (LEVEL == LOG_WARNING) { WARN_MSG(__VA_ARGS__); } \
    else if (LEVEL == LOG_NOTICE)  { NOTICE_MSG(__VA_ARGS__); } \
    else if (LEVEL == LOG_INFO)    { INFO_MSG(__VA_ARGS__); } \
    else if (LEVEL == LOG_DEBUG)   { DEBUG_MSG(__VA_ARGS__); } \
    else { INVALID_LEVEL_MSG(__VA_ARGS__); } \

#endif
