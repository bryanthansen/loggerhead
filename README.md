# log.h

author: Bryant Hansen

license: LGPL

A console logger for C programs


## Design Concept

I need a standard, go-to logger for all general C libraries/programs/drivers

    LOG(LOG_INFO, "I'm your program and I just did something interesting")

Requirements:

 * Non-copyleft licence (can use it for both personal and work projects)
   (Only citing requirement)
 * Standard debug levels (CRIT, ERR, WARN, INFO...)
 * Timestamps with millisecond accuracy, in the sortable YYYYmmdd_HHMMSS.NNN
   format
 * Color syntax highlighting
 * Variable argument processing
 * Performed as-much-as-possible in preprocessor code, not requiring compiled
   modules, only function substitutions
   (e.g. strftime in the timestamp will be precompiled into target programs
   via preprocessor defines)

Searching around, I can't find anything that fits all requirements.


## Shell Colors

    #define COLOR_RESET              "\033[0m"
    #define BOLD                     "\033[1m"
    #define BLACK_TEXT               "\033[30;1m"
    #define RED_TEXT                 "\033[31;1m"
    #define GREEN_TEXT               "\033[32;1m"
    #define YELLOW_TEXT              "\033[33;1m"
    #define BLUE_TEXT                "\033[34;1m"
    #define MAGENTA_TEXT             "\033[35;1m"
    #define CYAN_TEXT                "\033[36;1m"
    #define WHITE_TEXT               "\033[37;1m"
    #define RED_INVERTED_TEXT        "\033[41;1m"
    #define RED_FLASHING_TEXT        "\033[5;31m"
    #define RED_BG_FLASHING_TEXT     "\033\e[5;41m"

The following bash script should be able to print all combinations:

    nocolor="\e[0m"
    for n2 in `seq 27 54` ; do
       for n1 in `seq 0 16` ; do
           color="\e[${n1};${n2}m"
           echo -n -e "${color}\\${color}${nocolor}  "
       done
       echo ""
    done

Or formatted as a convenient copy-paste 1-liner with a certainly-invalid line
length:

    nocolor="\e[0m" ; for n2 in `seq 27 54` ; do for n1 in `seq 0 16` ; do color="\e[${n1};${n2}m" ; echo -n -e "${color}\\${color}${nocolor}  " ; done ; echo "" ; done

What should be visible on successful execution:
![Shell Colors](./doc/2019-02-14_20-01-30_1440x900b_shellcolors.png)

Another view of a more-limited-range of shell character settings, showing both
unselected (above) and selected(below) text:

![Shell Colors](./doc/shellcolors_unselected.png)
![Shell Colors](./doc/shellcolors_selected.png)


## Debug Levels

The following debug levels have been shamelessly-stolen from the bowels of Linux

/usr/include/sys/syslog.h -- part of glibc

    LOG_EMERG	0	/* system is unusable *
    LOG_ALERT	1	/* action must be taken immediately *
    LOG_CRIT	2	/* critical conditions *
    LOG_ERR		3	/* error conditions *
    LOG_WARNING	4	/* warning conditions *
    LOG_NOTICE	5	/* normal but significant condition *
    LOG_INFO	6	/* informational *
    LOG_DEBUG	7	/* debug-level messages *

## Dependencies

log.h contains the following includes:

    #include <stdio.h>
    #include <time.h>

It was considered to add syslog.h, to avoid duplicate definitions of the
various log levels.

